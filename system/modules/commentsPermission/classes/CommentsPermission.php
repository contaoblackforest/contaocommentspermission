<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   CommentsPermission
 * @file      CommentsPermission.php
 * @author    Sven Baumann <baumann.sv@gmail.com>
 * @license   http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


namespace Contao;


/**
 * Class CommentsPermission
 *
 * @package Contao
 */
class CommentsPermission extends Backend
{
    public function __construct()
    {
        parent::__construct();

        $this->import('BackendUser', 'User');
    }


    public function checkPermission()
    {
        if ($this->User->isAdmin) {
            return;
        }

        $root = array();

        $objComments = Database::getInstance()
            ->query('SELECT id, source, parent FROM tl_comments');

        if ($objComments->count()) {

            while ($objComments->next()) {

                $root[] = $this->havePermission($objComments->row());
            }
        }

        $GLOBALS['TL_DCA']['tl_comments']['list']['sorting']['root'] = $root;
    }


    protected function havePermission($arrComments)
    {
        $strSource = str_replace('tl_', '', $arrComments['source']);

        $strModel = \Model::getClassFromTable($arrComments['source']);

        /** @var \Model $objSource */
        $objSource = $strModel::findByPk($arrComments['parent']);

        if (!$objSource) {
            return 0;
        }

        $compare = array
        (
            'calendar' => 'calendars'
        );

        if (!$this->User->$strSource) {
            if (array_key_exists($strSource, $compare)) {
                $strSource = $compare[$strSource];
            }
        }

        /** @var \Model $objParentSource */
        if (!$this->User->$strSource
            && $objParentSource = $objSource->getRelated('pid')
        ) {
            $strSource = str_replace('tl_', '', $objParentSource->getTable());
        }

        if (!$this->User->$strSource) {
            if (array_key_exists($strSource, $compare)) {
                $strSource = $compare[$strSource];
            }
        }

        $arrPermissons = array();
        if ($this->User->$strSource) {
            $arrPermissons = $this->User->$strSource;
        }

        if (!empty($arrPermissons)
            && in_array($objSource->pid, array_values($arrPermissons))
        ) {
            return $arrComments['id'];
        }

        return 0;
    }
}
