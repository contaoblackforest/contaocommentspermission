<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package CommentsPermission
 * @file      tl_comments.php
 * @author    Sven Baumann <baumann.sv@gmail.com>
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */

//Adding Permission to comments list view
$GLOBALS['TL_DCA']['tl_comments']['config']['onload_callback'][] = array('CommentsPermission', 'checkPermission');
